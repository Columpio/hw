object PascalTriange {
    @annotation.tailrec
    def fact(n: Int, acc: Int): Int =
        if (n <= 1)
            acc
            else fact(n - 1, acc * n)

    @annotation.tailrec
    def productFromY2X(y: Int, x: Int, acc: Int): Int =
        if (y > x)
            acc
            else productFromY2X(y + 1, x, acc * y)

    def getPascalElement(x: Int, y: Int): Int = {
        /** Combinations from x to y */
        productFromY2X(y + 1, x, 1) / fact(x - y, 1)
    }

    def main(args: Array[String]): Unit =
        println(getPascalElement(4, 2))
}

object ParannthesesBalance {
    def check(seq: List[Char]): Boolean = {
        @annotation.tailrec
        def iter(seq: List[Char], opened: Int): Boolean =
            if (seq.isEmpty)
                opened == 0
            else if (seq.head == '(')
                iter(seq.tail, opened + 1)
            else if (seq.head == ')')
                if (opened == 0)
                    false
                else iter(seq.tail, opened - 1)
            else iter(seq.tail, opened)
        iter(seq, 0)
    }

    def main(args: Array[String]): Unit =
        println(check("(())".toList))
}

object exchangeCoins {
    def cc(amount: Double, vals: List[Double]): Int =
        if (amount == 0.0)
            1
        else if ((amount < 0.0) || (vals.isEmpty))
            0
        else cc(amount, vals.tail) + cc(amount - vals.head, vals)

    def main(args: Array[String]): Unit =
        println(cc(100, List(0.5, 1, 2, 5, 10, 20, 50, 100)))
}

object Roots {
    def sign(x: Double): Int =
        if (x < 0) -1 else if (x > 0) 1 else 0
    def findRoots(seq: Array[Double]): List[Double] =
        if (seq.length == 3) {
            // Sqr
            val a: Double = seq(0)
            val b: Double = seq(1)
            val c: Double = seq(2)
            val D: Double = b * b - 4 * a * c
            if (D > 0) {
                List((-b - math.sqrt(D)) / (2 * a),
                     (-b + math.sqrt(D)) / (2 * a))
            } else if (D == 0) {
                List(-b / (2 * a))
            } else Nil
        } else if (seq.length == 4) {
            // Cube
            if (seq(0) == 0) { // Square! It's a trap!
                findRoots(seq.drop(1)) // ~ array.tail
            } else if (seq(0) == 1) {
                val a: Double = seq(1)
                val b: Double = seq(2)
                val c: Double = seq(3)
                val q: Double = (a * a - 3 * b) / 9
                val r: Double = (2 * a * a * a - 9 * a * b + 27 * c) / 54
                val r_2_min_q_3: Double = r * r - q * q * q

                if (r_2_min_q_3 < 0) { // Three R Roots
                    val t: Double = math.acos(r / math.sqrt(q * q * q)) / 3
                    val _2_pi_3: Double = 2 * math.Pi / 3

                    List(2 * math.sqrt(q) * math.cos(t) - a / 3,
                         2 * math.sqrt(q) * math.cos(t + _2_pi_3) - a / 3,
                         2 * math.sqrt(q) * math.cos(t - _2_pi_3) - a / 3)
                } else { // One or maybe 2 R roots
                    val A: Double = sign(r) *
                                    math.pow(math.abs(r) +
                                        math.sqrt(r_2_min_q_3), 1/3)
                    val B: Double = if (A == 0) 0 else q / A

                    val x1 = (A + B) - a / 3
                    if (A == B && A != 0)
                        List(x1, -A - a / 3)
                    else
                        List(x1)
                }
            } else {
                findRoots(seq.map(_ / seq(0)).toArray)
            }
        } else {
            println("No support")
            Nil
        }
    def main(args: Array[String]): Unit =
        println(findRoots(Array(31, -2, 0, 1)))
}

object ReverseList {
    @annotation.tailrec
    def iter[A](seq: List[A], acc: List[A]): List[A] =
        if (seq.isEmpty)
            acc
        else iter(seq.tail, seq.head :: acc)

    def reverse[A](seq: List[A]): List[A] = iter(seq, Nil)

    def main(args: Array[String]): Unit =
        println(reverse(List(1, 2, 3, -1, -2, 0)))
}

object add2End {
    def add[A](seq: List[A], item: A): List[A] =
        if (seq.isEmpty)
            item :: Nil
        else seq.head :: add(seq.tail, item)

    def main(args: Array[String]): Unit =
        println(add(List(1, 2, 3, -1, -2, 0), 5))
}

object length {
    @annotation.tailrec
    def iter(seq: List[Int], acc: Int): Int =
        if (seq.isEmpty)
            acc
        else iter(seq.tail, acc + 1)

    def len(seq: List[Int]): Int = iter(seq, 0)

    def main(args: Array[String]): Unit =
        println(len(List(1, 2, 3, -1, -2, 0)))
}

object summator {
    @annotation.tailrec
    def iter(seq: List[Int], acc: Int): Int =
        if (seq.isEmpty)
            acc
        else iter(seq.tail, acc + seq.head)

    def summ(seq: List[Int]): Int = iter(seq, 0)

    def main(args: Array[String]): Unit =
        println(summ(List(1, 2, 3, -1, -2, 0)))
}

object badFilterFunc {
    def filter(seq: List[Int]): List[Int] = {
        if (seq.isEmpty)
            Nil
        else if (seq.head > 0) // criteria
            seq.head :: filter(seq.tail)
        else filter(seq.tail)
    }

    def main(args: Array[String]): Unit =
        println(filter(List(1, 2, 3, -1, -2, 0)))
}

object filterFunc {
    def filter[A](pred: A => Boolean, seq: List[A]): List[A] =
        if (seq.isEmpty)
            Nil
        else if (pred(seq.head))
            seq.head :: filter(pred, seq.tail)
        else filter(pred, seq.tail)

    def plusp(x: Int): Boolean = (x > 0)

    def main(args: Array[String]): Unit =
        println(filter(plusp, List(1, 2, 3, -1, -2, 0)))
}
