object SetRealisation {
    type Set = Int => Boolean
    def contains(s: Set, elem: Int): Boolean = s(elem)

    def singletonSet(elem: Int): Set = x =>
        x == elem

    def union(s: Set, t: Set): Set = x =>
        contains(s, x) || contains(t, x)

    def intersect(s: Set, t: Set): Set = x =>
        contains(s, x) && contains(t, x)

    def diff(s: Set, t: Set): Set = x =>
        contains(s, x) && !contains(t, x)

    def filter(s: Set, p: Int => Boolean): Set = x =>
        contains(s, x) && p(x)

    def takeAll(s: Set): String = {
        val bound = 1000
        def toString(s: Set): String = {
            val xs = for (i <- -bound to bound if contains(s, i)) yield i
            xs.mkString("{", ",", "}")
        }
        toString(s)
    }

    def forall(s: Set, p: Int => Boolean): Boolean = {
        val bound = 1000

        @annotation.tailrec
        def iter(a: Int): Boolean =
            if (a > bound)
                true
            else if (contains(s, a) && !p(a))
                false
            else iter(a + 1)

        iter(-bound)
    }

    def exists(s: Set, p: Int => Boolean): Boolean = {
        val bound = 1000

        @annotation.tailrec
        def iter(a: Int): Boolean =
            if (a > bound)
                false
            else if (contains(s, a) && p(a))
                true
            else iter(a + 1)

        iter(-bound)
    }

    def map(s: Set, f: Int => Int): Set = {
        val bound = 1000

        def iter(a: Int): Set =
            if (a > bound)
                x => false // empty set
            else if (contains(s, a))
                union(singletonSet(f(a)), iter(a + 1))
            else iter(a + 1)

        iter(-bound)
    }

    // for easy testing
    def __mk_from_list(l: List[Int]): Set =
        if (l.isEmpty)
            x => false
        else union(singletonSet(l.head), __mk_from_list(l.tail))

    def main(args: Array[String]): Unit = {
        assert(contains(singletonSet(5), 4) == false)
        assert(contains(singletonSet(5), 5))
        assert(contains(union(singletonSet(5), singletonSet(4)), 4))
        assert(contains(union(singletonSet(5), singletonSet(4)), 5))
        assert(contains(union(singletonSet(5), singletonSet(4)), 7) == false)
        assert(contains(__mk_from_list(List(1,2,3)), 3))
        assert(contains(__mk_from_list(List(1,2,3)), 4) == false)
        assert(contains(intersect(__mk_from_list(List(1,2)),
                                  __mk_from_list(List(2,3))), 2))
        assert(contains(intersect(__mk_from_list(List(1,2)),
                                  __mk_from_list(List(2,3))), 1) == false)
        assert(contains(diff(__mk_from_list(List(1,2)),
                             __mk_from_list(List(2,3))), 1))
        assert(contains(diff(__mk_from_list(List(1,2)),
                             __mk_from_list(List(2,3))), 2) == false)
        assert(contains(filter(__mk_from_list(List(1,2,3,4)),
                               {e: Int => e % 2 == 0}), 4))
        assert(contains(filter(__mk_from_list(List(1,2,3,4)),
                               {e: Int => e % 2 == 0}), 3) == false)
        assert(forall(__mk_from_list(List(1,2,3,4)), {e: Int => e < 5}))
        assert(forall(__mk_from_list(List(1,2,3,4)),
                                     {e: Int => e < 4}) == false)
        assert(exists(__mk_from_list(List(1,2,3,4)), {e: Int => e > 3}))
        assert(exists(__mk_from_list(List(1,2,3,4)),
                                     {e: Int => e < 0}) == false)
        assert(contains(map(__mk_from_list(List(1,2,3,4)), {_*2}), 8))
    }
}
