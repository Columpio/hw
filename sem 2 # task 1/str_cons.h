#include <string.h>
#include <stdlib.h>
#include <stdio.h>

struct str_cons {
    char* str;
    int car;
    struct str_cons *cdr;
};
typedef struct str_cons str_cons;

void str_cons_add(str_cons* *list, char* str, int num)
{
    str_cons* lastPtr = malloc(sizeof(str_cons));
    char* s_cp = calloc(strlen(str) + 1, sizeof(char));
    if ((lastPtr == NULL) || (s_cp == NULL)) {
        printf("Not enough memory!\n");
        return;
    }

    strcpy(s_cp, str);

    lastPtr->car = num;
    lastPtr->str = s_cp;
    lastPtr->cdr = *list;

    *list = lastPtr;
}

void str_cons_free(str_cons* *list)
{
    str_cons *curPtr, *tempPtr;
    curPtr = *list;

    while (curPtr != NULL) {
        tempPtr = curPtr;
        curPtr = curPtr->cdr;
        free(tempPtr->str);
        free(tempPtr);
    }
}

void print_str_cons(str_cons* list)
{
    if (list == NULL) {
        printf("NULL\n");
    } else if (list->cdr == NULL) {
        printf("%s : %d\n", list->str, list->car);
    } else {
        printf("%s : %d -> ", list->str, list->car);
        print_str_cons(list->cdr);
    }
}
