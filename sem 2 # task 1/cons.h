#include <stdio.h>
#include <stdlib.h>

/* CONS DEFINITION */
struct cons {
    int car_i;
    int* mem;
};
typedef struct cons cons;

void cons_add(cons* list, const int num)
{
    list->mem[++list->car_i] = num;
}

int cons_pop(cons* list)
{
    if (list->car_i == -1) {
        printf("Error! Nothing to pop.\n");
        return -1;
    } else {
        return list->mem[list->car_i--];
    }
}

void print_cons(cons* list)
{
    if (list->car_i == -1) {
        printf("NULL\n");
    } else {
        printf("%d", list->mem[list->car_i]);

        int i;
        for (i = list->car_i - 1; i >= 0; i--) {
            printf(" -> %d", list->mem[i]);
        }
    }
}

void cons_free(cons* list)
{
    free(list->mem);
    free(list);
}

cons* cons_init(const int cons_size)
{
    cons* NewCons = calloc(1, sizeof(cons));
    int* temp = calloc(cons_size, sizeof(int));
    if (temp == NULL) {
        printf("Not enough memory!\n");
        return NULL;
    }
    NewCons->mem = temp;
    NewCons->car_i = -1;

    return NewCons;
}
