#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cons.h"
#include "str_cons.h"

#define   MEMSIZE 262144
#define STACKSIZE 10000
#define  CODESIZE 5000

#define FILENAME_LEN 30
#define     LINE_LEN 78

#define NUM_OF_OPERATORS 9
#define INT_OPERAND_CODE 1
#define STR_OPERAND_CODE 2
#define RET_CODE 0
#define  LD_CODE 1
#define  ST_CODE 2
#define LDC_CODE 3
#define ADD_CODE 4
#define SUB_CODE 5
#define CMP_CODE 6
#define JMP_CODE 7
#define  BR_CODE 8

/* FEEL FREE TO MODIFY */
// "Running itself" functions can also be modified
inline char functorIndexP(int i)
{
    /*      for indexes:   0    1   2   3    4    5    6    7    8
            of commands:   ret, ld, st, ldc, add, sub, cmp, jmp, br
                returns:   0    1   1   1    0    0    0    2    2
      if treat as bools:   F    T   T   T    F    F    F    T    T
     (as ld, st, ldc, jmp and br need operand (they are functors)) */
     if (i == LD_CODE || i == ST_CODE || i == LDC_CODE) { // need an integer operand
        return INT_OPERAND_CODE;
     } else if (i == JMP_CODE || i == BR_CODE) { // need a string operand
        return STR_OPERAND_CODE;
    } else { // don't need any operand
        return 0; // A MUST
    }
}

/* SYS functions and stuff */
struct meta_code {
    unsigned int command:4;
    int operand;
};
typedef struct meta_code meta_code;

void meta_code_free(meta_code** CODE)
{
    meta_code *tempPtr;
    int i;

    for (i = 0, tempPtr = CODE[i] ; tempPtr != NULL; i++, tempPtr = CODE[i]) {
        free(tempPtr);
    }
    free(CODE);
}

void str_array_free(char** s_arr, const int len)
{
    int i;

    for (i = 0; i < len; i++) {
        free(s_arr[i]);
    }
    free(*s_arr);
}

/* Parse functions */
void str_trim(char* s)
{
    int i = 0;

    while (s[i] == ' ') {
        i++;
    }
    strcpy(s, s + i);

    while (s[i]) { // find \0
        i++;
    }
    if (i) {
        --i;
        while (s[i] == ' ') {
            --i;
        }
        s[i + 1] = 0;
    }
}

int labelToNum(char* label, char** lNames, int* lIndexes)
{
    int i;

    for (i = 1; i < lIndexes[0]; i++) {
        if (!strcmp(label, lNames[i])) {
            return lIndexes[i];
        }
    }

    return -1; // no label found
}

int parse(char* input, meta_code* CODE_line, int k, char** lNames, int* lIndexes, str_cons* *lPromises)
{
    char* label = strchr(input, ':');
    if (label != NULL) { // label found
        int len = strlen(input) - strlen(label);
        char* t_str = calloc(len+1, sizeof(char));
        strncpy(t_str, input, len);
        t_str[len] = 0;
        lNames[lIndexes[0]] = t_str;

        lIndexes[lIndexes[0]++] = k;
        input = label + 1;
        input[strlen(label)-1] = 0;
    }

    str_trim(input); // '  a b  ' -> 'a b'

    if (input[0]) { // operators left
        char* temp_str = strchr(input, ' ');
        int i;
        char* opers[NUM_OF_OPERATORS] = {"ret", "ld", "st", "ldc", "add",
                                         "sub", "cmp", "jmp", "br"};

        if (temp_str == NULL) { // no ' ', so no operands
            for (i = 0; i < NUM_OF_OPERATORS; i++) {
                if (!strcmp(input, opers[i])) {
                    if (functorIndexP(i)) {
                        printf("Invalid number of operands on line: %d\n", k);
                        return 0; // ignore
                    } else {
                        CODE_line->command = i;
                        return 1;
                    }
                }
            }
            printf("Invalid operator on line: %d\n", k);
            return 0; // don't look at bad operator
        } else {
            input[strlen(input) - strlen(temp_str)] = 0; // operator
            temp_str = temp_str + 1;
            for (i = 0; i < NUM_OF_OPERATORS; i++) {
                if (!strcmp(input, opers[i])) {
                    if (!functorIndexP(i)) {
                        printf("Invalid number of operands on line: %d\n", k);
                        return 0; // ignore
                    } else if (functorIndexP(i) == INT_OPERAND_CODE) { // needs integer argument
                        CODE_line->command = i;
                        CODE_line->operand = atoi(temp_str); // atoi(" 12junk") = 12
                    } else { // functorIndexP(i) == 2, so needs string argument
                        CODE_line->command = i;
                        int labelLine = labelToNum(temp_str, lNames, lIndexes);
                        if (labelLine == -1) {      // label is unknown, so
                            CODE_line->operand = 0; // will be replaced after full analyses
                            str_cons_add(lPromises, temp_str, k); // promise to replace
                        } else {
                            CODE_line->operand = labelLine;
                        }
                    }
                    return 1;
                }
            }
            printf("Invalid operator on line: %d\n", k);
            return 0; // don't look at bad operator
        }
    } else {
        return 0; // zero string
    }
}

int createCode(meta_code** CODE, char* filename, char** lNames, int* lIndexes,
               str_cons* *lPromises)
{
    FILE* fl = fopen(filename, "r");
    if (!fl) {
        printf("File I/O Error\n");
        return -1;
    }
    int k = 0; // CODE[k]
    char input[LINE_LEN] = {0};
    int len = 0; // position in input
    char c;

    while (!feof(fl)) {
        c = fgetc(fl);

        if (c == '\n') {
            if (input[0]) {
                meta_code* tempLine = malloc(sizeof(meta_code));
                if (tempLine == NULL) {
                    printf("Not enough memory!\n");
                    return -1;
                }
                tempLine->command = 0;
                tempLine->operand = 0;
                CODE[k] = tempLine;
                if (parse(input, CODE[k], k, lNames, lIndexes, lPromises)) { // if real line
                    ++k;
                }
                input[0] = 0;
            }
            len = 0;
        } else if (len != -1) {
            if (c == ';') {
                len = -1; // flag of comment
            } else if (c != '\t') {
                input[len++] = c;
                input[len] = 0;
            }
        }
    }

    fclose(fl);

    return k; // CODE length
}

void labelsNamesToNums(meta_code** CODE, char* filename, char** lNames, int* lIndexes,
                       str_cons* *lPromises)
{
    str_cons* tempPtr;

    while (*lPromises != NULL) {
        tempPtr = *lPromises;
        int labelLine = labelToNum(tempPtr->str, lNames, lIndexes);

        if (labelLine == -1) {
            printf("Error! No such label: %s", tempPtr->str);
        } else {
            meta_code* tempLine = CODE[tempPtr->car];
            tempLine->operand = labelLine;
        }

        *lPromises = (*lPromises)->cdr;
        free(tempPtr);
    }
}

/* Running itself */
void HAPPY_FINISH(cons* STACK)
{
    printf("Execution complete. Stack:\n");
    print_cons(STACK);

    cons_free(STACK);
}

void RUN_PROGRAM(meta_code** CODE, const int codeLength)
{
    // MEM and STACK init
    int MEM[MEMSIZE] = {0};
    cons* STACK = cons_init(STACKSIZE);
    int k = 0; // current code-line index
    meta_code* curentLine;
    int last, prev; // for CMP

    while (1) { // like REPL
        if (k >= codeLength) {
            HAPPY_FINISH(STACK);
            return;
        }
        curentLine = CODE[k];
        switch (curentLine->command) {
            case RET_CODE:
                HAPPY_FINISH(STACK);
                return;
            case LD_CODE:
                cons_add(STACK, MEM[curentLine->operand]);
                break;
            case ST_CODE:
                MEM[curentLine->operand] = cons_pop(STACK);
                break;
            case LDC_CODE:
                cons_add(STACK, curentLine->operand);
                break;
            case ADD_CODE:
                cons_add(STACK, cons_pop(STACK) + cons_pop(STACK));
                break;
            case SUB_CODE:
                cons_add(STACK, cons_pop(STACK) - cons_pop(STACK));
                break;
            case CMP_CODE:
                last = cons_pop(STACK);
                prev = cons_pop(STACK);
                cons_add(STACK, last > prev ? 1 : last < prev ? -1 : 0);
                break;
            case JMP_CODE:
                k = curentLine->operand;
                continue;
            case BR_CODE:
                if (cons_pop(STACK)) {
                    k = curentLine->operand;
                    continue;
                }
                break;
        }
        ++k;
    }
}

int main()
{
    // Introduction
    char filename[FILENAME_LEN] = {0};
    printf("File name: ");
    scanf("%s", &filename);

    // Future code structure
    meta_code** CODE = calloc(CODESIZE, sizeof(meta_code));
    if (CODE == NULL) {
        printf("Not enough memory!");
        return -1;
    }

    // Some stuff for labels and goto (jmp and br)
    char* labels_names[CODESIZE];
    int labels_indexes[CODESIZE] = {0};
    labels_indexes[0] = 1; // index to start
    str_cons* lPromises = NULL; // label names and index to place labelToNum result

    // all 'jmp' and 'br' will have '0' arg
    int codeLen = createCode(CODE, filename, labels_names, labels_indexes, &lPromises);

    // making all gotos work (final replace of strings-labels with command number in CODE)
    labelsNamesToNums(CODE, filename, labels_names, labels_indexes, &lPromises); // will del lPromises
    str_array_free(labels_names, labels_indexes[0]); // 'cause all names have been already converted to nums

    RUN_PROGRAM(CODE, codeLen);

    /* Free everything */
    meta_code_free(CODE);

    return 0;
}
