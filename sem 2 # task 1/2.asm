; sum even fibon <= 2 000 000
ldc 2000000
st 0 ; MEM[0] = 2*10**6
ldc 0
st 1 ; fib(0)
ldc 1
st 2 ; fib(1)
ldc 0
st 3 ; sum
jmp cycle

Write:
	ld 2
	ld 3
	add
	st 3
	jmp cycle

even?:
	ldc 0
	ld 4
	cmp ; n == 0?
	br Next-even? ; !=
	jmp Write

Next-even?:	
	ldc 1
	ld 4
	cmp ; n > 1
	br Next-even2? ; != 1
	jmp cycle ; == 1

Next-even2?:
	ldc 2
	ld 4
	sub ; n - 2
	st 4
	jmp even?

cycle:
	ld 1 ; fib(n)
	ld 2 ; fib(n+1)
	add ; fib(n+2)
	ld 2 ; fib(n+1)
	st 1 ; fib(n+1) -> 1
	st 2 ; fib(n+2) -> 2
	
	ldc 1
	ld 0
	ld 2
	cmp ; fib(n+2) > 2e6
	add ; cmp++
	br End
	
	ld 2
	st 4 ; MEM[4] = fib(n+2)
	jmp even?

End:
	ld 3
	ret
	