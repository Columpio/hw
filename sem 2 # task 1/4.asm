; 208574561 max prime divisor
; 1471
ldc 208574561
st 0 ; MEM[0] = our num
ldc 1
ld 0
sub
st 1 ; our--
ld 0
st 2

div?:
	; mem[2] by mem[1]
	ldc 1
	ld 1
	cmp
	br next_div?
	jmp End

next_div?: ld 1
	ld 2
	cmp
	st 3
	ld 3
	br next_div2?
	ld 1 ; 19
	st 0
	ld 0
	st 2
	ldc 1
	ld 1
	sub
	st 1
	jmp div?

next_div2?:
	ldc 1
	ld 3
	sub ; 0 if mem[2] > mem[1]
	br newNum
	ld 1
	ld 2
	sub
	st 2
	jmp div?

newNum:
	ldc 1
	ld 1
	sub
	st 1
	
	ld 0
	st 2
	jmp div?

End:
	ld 0
	ret