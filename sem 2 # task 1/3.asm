; 104743
ldc 10001
st 0 ; MEM[0] = 10001 ( max i )
ldc 1
st 5 ; MEM[5] = 1 ( cur i )
ldc 2 ; first prime
st 1 ; MEM[1] = 2

prime?: ; -> MEM[1:4] = [x, x-1, x]
	ldc 1
	ld 1
	sub ; x - 1
	st 2 ; MEM[2] = x - 1
	ld 1
	st 3 ; MEM[3] init with MEM[0]

div-check-divisor?: ; MEM[2] == 1?
	ldc 1
	ld 2
	cmp ; MEM[2] == 1 ? 0 : 1
	br div-check-dividend? ; MEM[2] >= 2
	
	; prime found
	ld 0 ; max i
	ld 5 ; cur i
	cmp  ; i == max ? 0 : -1
	br call-next-prime ; i < max
	
	; i == max -> finish
	ld 1
	ret

reduce-dividend:
	ld 2
	ld 3
	sub
	st 3 ; MEM[3] -= MEM[2]
	
div-check-dividend?: ; MEM[3] >= MEM[2]?
	ld 2 ; >  < =
	ld 3 ; 1 -1 0
	cmp ; MEM[3] == MEM[2] ? 0 : +-1
	st 4 ; MEM[4] = cmp result
	ld 4
	br bad-mod ; MEM[3] <> MEM[2]
	jmp next-number

call-next-prime:
	ld 5
	ldc 1
	add
	st 5 ; (curr i): MEM[5]++
	
next-number:
	ld 1
	ldc 1
	add
	st 1 ; (x): MEM[1]++
	jmp prime?
	
bad-mod: ; x % y: 1 if x > y else -1
	ldc -1
	ld 4
	cmp ; cmp_res == -1 ? 0 : 1
	br reduce-dividend ; x -= y
	
	; 0 < x < y
	ld 1
	st 3 ; MEM[3] = x again
	ldc 1
	ld 2
	sub
	st 2 ; MEM[2]--
	jmp div-check-divisor?
