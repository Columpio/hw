#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* MY HASH */
unsigned int hash(char* key, size_t len)
{
    unsigned int hash = 0, i;

    for(i = 0; i < len; i++) {
        hash += key[i];
        hash += hash << 10;
        hash ^= hash >> 6;
    }

    hash += hash << 3;
    hash ^= hash >> 11;
    hash += hash << 15;

    return hash;
}

/* CONS INTERFACE */
struct cons {
    char* str;
    int car;
    struct cons *cdr;
};
typedef struct cons cons;

void cons_free(cons* *list)
{
    cons *curPtr, *tempPtr;
    curPtr = *list;

    while (curPtr != NULL) {
        tempPtr = curPtr;
        curPtr = curPtr->cdr;
        free(tempPtr);
    }
}

int cons_length(cons* *list)
{
    int length = 0;
    cons *curPtr;
    curPtr = *list;

    while (curPtr != NULL) {
        curPtr = curPtr->cdr;
        ++length;
    }

    return length;
}

void print_cons(cons* list)
{
    if (list == NULL) {
        printf("NULL\n");
    } else {
        printf("%s : %d\n", list->str, list->car);
        if (list->cdr != NULL) {
            print_cons(list->cdr);
        }
    }
}

/* HASH TABLE INTERFACE */
struct hash_table {

    cons** table;
    int hash_div;
    unsigned int (*hash_func) (char*, size_t);
};
typedef struct hash_table hash_table;

// Tables
void nullfill_hash_table(hash_table* ht)
{
    int i, n = ht->hash_div;

    for (i = 0; i < n; i++) {
        (ht->table)[i] = NULL;
    }
}

void print_table(hash_table* ht)
{
    int i, n = ht->hash_div;
    cons** table = ht->table;

    for (i = 0; i < n; i++) {
        if (table[i] != NULL) {
            print_cons(table[i]);
        }
    }
}

hash_table* create_hash_table(unsigned int (*hash_func) (char*, size_t), const int hash_div)
{
    hash_table* ht = malloc(sizeof(hash_table));
    if (ht == NULL) {
        printf("Not enough memory!\n");
        return NULL;
    }

    ht->table = malloc(sizeof(cons) * hash_div);
    if (ht->table == NULL) {
        printf("Not enough memory!\n");
        return NULL;
    }
    ht->hash_div = hash_div;
    ht->hash_func = hash_func;

    nullfill_hash_table(ht);

    return ht;
}

void del_hash_table(hash_table* ht)
{
    int i, n = ht->hash_div;
    cons** table = ht->table;

    for (i = 0; i < n; i++) {
        cons_free(&table[i]);
    }

    free(table);
    free(ht);
}

void pprint_stat(int NnonzeroConses, int SummarConsLength, int min_length, int max_length)
{
    printf("####################################\n");
    printf("The number of nonzero conses: %d\n", NnonzeroConses);
    printf("The number of elements: %d\n", SummarConsLength);
    printf("The average length of conses: %.3f\n",
           NnonzeroConses ? ((double) SummarConsLength) / NnonzeroConses : 0);
    printf("The minimum length of the cons: %d\n", min_length);
    printf("The maximum length of the cons: %d\n", max_length);
    printf("####################################\n\n");
}

void print_statistics(hash_table* ht)
{
    int min_length = 0, max_length = 0;
    int curLength, NnonzeroConses = 0, SummarConsLength = 0;
    int i, n = ht->hash_div;
    cons** table = ht->table;

    for (i = 0; i < n; i++) {
        if (table[i] != NULL) {
            ++NnonzeroConses;
            curLength = cons_length(&table[i]);
            SummarConsLength += curLength;
            if (curLength < min_length || !min_length) {
                min_length = curLength;
            }
            if (curLength > max_length) {
                max_length = curLength;
            }
        }
    }

    pprint_stat(NnonzeroConses, SummarConsLength, min_length, max_length);
}

// Elements
cons* find_element(cons** list, char* str)
{
    // first -> first; not first -> previous; not found -> NULL
    cons *curPtr, *prevPtr;

    if (*list != NULL) {
        if (strcmp((*list)->str, str)) {
            curPtr = *list;
            do {
                prevPtr = curPtr;
                curPtr = curPtr->cdr;
            } while ((curPtr != NULL) && (curPtr->str != str));

            if (curPtr != NULL) {
                return prevPtr;
            }
        } else {
            return *list;
        }
    }

    return NULL;
}

cons* true_find_element(cons** list, char* str)
{
    cons* foundPtr = find_element(list, str);

    if (foundPtr != NULL) {
        if (strcmp(foundPtr->str, str)) {
            foundPtr = foundPtr->cdr;
        }
        return foundPtr;
    } else {
        return NULL;
    }
}

void set_element(hash_table* ht, char* str, const int value)
{
    int index = (ht->hash_func)(str, strlen(str)) % (ht->hash_div);
    cons** table = ht->table;
    cons* curPtr = true_find_element(&table[index], str);

    if (curPtr != NULL) {
        curPtr->car = value;
    } else {
        cons *newPtr = malloc(sizeof(cons));
        if (newPtr == NULL) {
            printf("Not enough memory!\n");
            return;
        }
        newPtr->car = value;
        newPtr->str = str;

        if (table[index] == NULL) {
            newPtr->cdr = NULL;
            table[index] = newPtr;
        } else {
            cons *tempPtr = malloc(sizeof(cons));
            if (tempPtr == NULL) {
                printf("Not enough memory!\n");
                return;
            }
            tempPtr->car = table[index]->car;
            tempPtr->str = table[index]->str;
            tempPtr->cdr = table[index]->cdr;

            newPtr->cdr = tempPtr;
            table[index] = newPtr;
        }
    }
}

void del_element(hash_table* ht, char* str)
{
    int index = (ht->hash_func)(str, strlen(str)) % (ht->hash_div);
    cons** table = ht->table;
    cons* prevPtr = find_element(&table[index], str);
    cons *curPtr, *tempPtr;

    if (prevPtr != NULL) {
        if (strcmp(prevPtr->str, str)) {
            curPtr = prevPtr->cdr;
            tempPtr = curPtr;
            prevPtr->cdr = curPtr->cdr;
            free(tempPtr);
        } else {
            table[index] = prevPtr->cdr;
            free(prevPtr);
        }
    }
}

int get_element(hash_table* ht, char* str)
{
    int index = (ht->hash_func)(str, strlen(str)) % (ht->hash_div);
    cons** table = ht->table;
    cons* curPtr = true_find_element(&table[index], str);

    if (curPtr != NULL) {
        return curPtr->car;
    } else {
        return 0;
    }
}

int main(void)
{
    /*
    // testing
    hash_table *ht = create_hash_table(hash, 100);

    set_element(ht, "he", 3);
    set_element(ht, "hi", 4);
    print_statistics(ht);
    del_element(ht, "he");
    print_statistics(ht);
    del_element(ht, "hi");
    print_statistics(ht);
    */

    return 0;
}
