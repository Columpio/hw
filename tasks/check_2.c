#include <stdio.h>
#define task2_size 100

void task1(void)
{
    char c;
    char check;

    scanf("%c\n", &check);

    while ((c = getchar()) != NULL) {
        if (c != check) {
            putchar(c);
        }
    }
}

int task2(void)
{
    char num1[task2_size] = {0};
    char num2[task2_size] = {0};
    int i;

    scanf("%s", &num1);
    scanf("%s", &num2);

    for (i = task2_size - 1; i >= 0; i--) {
        if (num1[i] > num2[i]) {
            return 1;
        } else if (num1[i] < num2[i]) {
            return -1;
        }
    }

    return 0;
}

void task3(void)
{
    FILE *f = fopen("input.txt", "r");

    int x;
    char flag = 0; // 1 - /; 2 - //

    if(!f) {
        printf("File opening failed\n");
        return;
    }

    while ((x = fgetc(f)) != EOF) {
        if (x == '\n') {
            if (flag) {
                putchar('\n');
            }
            flag = 0;
        } else if (flag == 2) {
            putchar(x);
        } else if (x == '/') {
            if (flag * (1 - flag) == 0) {
                ++flag;
            }
        }
    }

    fclose(f);
}
