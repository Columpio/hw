#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define lenFuncArray 4 // how many functions are tested
#define waitTime 1700 // max processor time function can work

_Bool didntEnd = 0; // 1 if program works a lot
int past;           // To count time
int *TestedArray;   // original array, which we will fill with random numbers
int *cpTestedArray; // temporary  array, which is copy (cp) of TestedArray

/* TESTED FUNCTIONS HERE */
void O_n(int* A, const int len)
{
    int i, j, b = 0, k = -1;
    for (i = 0; i < len; i++) {
        if (A[i] > k) {
            k = A[i];
        }
    } // k = max(A)
    int *C = calloc(k + 1, sizeof(int));
    if (C == NULL) {
        printf("Not enough memory\n");
        return;
    }

    for (i = 0; i < len; i++) {
        ++C[A[i]];
    }

    for (j = 0; j <= k; j++) {
        for (i = 0; i < C[j]; i++) {
            A[b] = j;
            ++b;
        }
    }
}

void O_n_log_n(int* A, const int len)
{
    void swap(int *a, int *b)
    {
        int c = *a;
        *a = *b;
        *b = c;
    }

    void iter(int first, int last) {
        int i = first, j = last, x = A[(first + last) / 2];

        do {
            while (A[i] < x) {
                i++;
            }
            while (A[j] > x) {
                j--;
            }
            if(i <= j) {
                if (A[i] > A[j]) {
                    swap(&A[i], &A[j]);
                }
                i++;
                j--;
            }
        } while (i <= j);

        if (i < last) {
            iter(i, last);
        }
        if (first < j) {
            iter(first, j);
        }
    }

    iter(0, len - 1);
}

void O_n_2(int* A, const int len)
{
    void swap(int *a, int *b)
    {
        int c = *a;
        *a = *b;
        *b = c;
    }

    int i, j;

    for (i = 0; i < len - 1; i++) {
        for (j = 0; j < len - i - 1; j++) {
            if (A[j] > A[j + 1]) {
                swap(&A[j], &A[j + 1]);
            }
        }
        // This is not a part of algo
        if (clock() - past > waitTime) {
            didntEnd = 1;
            break;
        }
    }
}

void myQsort(int *a, const int len)
{
    void iter(const int start, const int end)
    {
        if ((start == end) || (start + 1 == end))
            return;

        int x = a[(start + end) / 2];
        int size_l = 0, size_r = 0;
        int *left = malloc(0), *right = malloc(0);
        int i;
        if ((left == NULL) || (right == NULL)) {
            printf("Not enough memory\n");
            return;
        }

        for (i = start; i < end; i++) {
            if (a[i] < x) {
                ++size_l;
            } else if (a[i] > x) {
                ++size_r;
            }
        }
        left = (int*) realloc(left, size_l * sizeof(int));
        right = (int*) realloc(right, size_r * sizeof(int));
        /*if (!(left && right)) {
            printf("1 Not enough memory\n");
            return;
        }*/
        size_l = 0; size_r = 0;
        for (i = start; i < end; i++) {
            if (a[i] < x) {
                left[size_l++] = a[i];
            } else if (a[i] > x) {
                right[size_r++] = a[i];
            }
        }
        for (i = 0; i < size_l; i++)
            a[start + i] = left[i];
        for (i = start + size_l; i < end - size_r; i++)
            a[i] = x;
        for (i = 0; i < size_r; i++)
            a[end - size_r + i] = right[i];
        free(left); free(right);
        iter(start, start + size_l);
        iter(end - size_r, end);
    }

    iter(0, len);
}

/* TESTING SYSTEM HERE */
// These functions are specialized for this task
// Of course, they can be more abstract
void fillWithRandom(int *array, const int width)
{
    int j;

    for (j = 0; j < width; j++) {
        array[j] = rand();
    }
}

void timeThem(void (*func_array[])(int*, int), int len_FA, int* origArr, int* cpArr,
              int tAlen)
{
    int i;

    printf("\n%12d", tAlen); // print lenght of array
    for (i = 0; i < len_FA; i++) {
        memcpy(cpArr, origArr, tAlen * sizeof(int)); // copying origin

        // Starting counting processor time
        past = clock();

        // Call tested function
        func_array[i](cpArr, tAlen);

        // Output
        putchar(' '); // between columns
        if (didntEnd) {
            printf("\tn/a (>%d)", waitTime);
            didntEnd = 0;
        } else {
            printf("%13d", clock() - past);
        }
    }
}

void printTableHead(char **headers, char *message, int len)
{
    int i;

    printf("%s", message);
    for (i = 0; i < lenFuncArray; i++) {
        printf("%14s", headers[i]);
    }
}

void freeArrays(int *array1, int *array2)
{
    free(array1);
    free(array2);
}

int main()
{
    int array_lens[] = {5,          // 0   0    0
                        10,         // 0   0    0
                        100,        // 0   0    0
                        1000,       // 0   0    2
                        100000,     // 1   13   n/a
                        1000000,    // 9   55   n/a
                        10000000,   // 69  537  n/a
                        100000000}; // 743 6278 n/a
    void (*func_array[])(int*, int) = {O_n, O_n_log_n, myQsort, O_n_2};
    char *func_names[] = {"O(n)", "O(n*log(n))", "myQuickSort", "O(n^2)"};
    TestedArray = (int*) malloc(sizeof(int));   // actually these two lines are
    cpTestedArray = (int*) malloc(sizeof(int)); // not needed. adding for security
    if ((TestedArray == NULL) || (cpTestedArray == NULL)) {
        printf("Not enough memory\n");
        return -1;
    }
    int i;

    printTableHead(func_names, "Array length", lenFuncArray);
    for (i = 0; i < sizeof(array_lens) / sizeof(array_lens[0]); i++) {
        TestedArray = (int*) realloc(TestedArray, array_lens[i] * sizeof(int));
        cpTestedArray = (int*) realloc(cpTestedArray, array_lens[i] * sizeof(int));
        if ((TestedArray == NULL) || (cpTestedArray == NULL)) {
            printf("Not enough memory\n");
            return -1;
        }
        fillWithRandom(TestedArray, array_lens[i]);
        timeThem(func_array, lenFuncArray, TestedArray, cpTestedArray, array_lens[i]);
    }

    freeArrays(TestedArray, cpTestedArray);

    return 0;
}
