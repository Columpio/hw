#include <stdio.h>
#define MAX_LEN 256

void char_xor(char* s, const char* p)
{
    int i;

    for (i = 0; i < MAX_LEN; i++) {
        s[i] ^= p[i];
    }
}

void test(void)
{
    int i, n;
    char a[MAX_LEN];
    char check[MAX_LEN] = {0};

    scanf("%d", &n);

    for (i = 0; i < n; i++) {
        scanf("%s", a);
        char_xor(check, a);
    }

    printf(check);
}

int main()
{
    test();

    return 0;
}
