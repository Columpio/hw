size_t strlen1(char* src)
{
    int i = 0;

    while (src[i] != '\0') {
        ++i;
    }

    return i;
}

int strcmp1(char* s1, char* s2)
{
    int i = 0;

    while (s1[i] != '\0' && s2[i] != '\0') {
        if (s1[i] > s2[i]) {
            return 1;
        } else if (s1[i] < s2[i]) {
            return -1;
        }
        ++i;
    }

    if (s1[i]) {
        return 1;
    } else if (s2[i]){
        return -1;
    } else {
        return 0;
    }
}

void strcpy1(char* dst, char* src)
{
    int i = 0;

    while (src[i] != '\0') {
        dst[i] = src[i];
        ++i;
    }

    dst[i] = '\0';
}

void strcat1(char* s1, const char* s2)
{
    int i = 0, j = 0;

    while (s1[i] != '\0') {
        ++i;
    }

    do {
        s1[i + j] = s2[j];
    } while (s2[j++] != '\0');
}
