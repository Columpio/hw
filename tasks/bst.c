#include <stdio.h>
#include <stdlib.h>

struct bst {
    int root;
    struct bst *left;
    struct bst *right;
};
typedef struct bst bst;

void add(bst* *tree, int num)
{
    if (*tree == NULL) {
        bst *list = malloc(sizeof(bst));
        if (list == NULL) {
            printf("Not enough memory\n");
            return;
        }
        list->root = num;
        list->left = NULL;
        list->right = NULL;

        *tree = list;
    } else if (num < (*tree)->root) {
        add(&((*tree)->left), num);
    } else if (num > (*tree)->root) {
        add(&((*tree)->right), num);
    }
}

int del_max_of_tree(bst* *tree) // only for del; waits for non null tree
{
    if ((*tree)->right == NULL) {
        int temp = (*tree)->root;
        *tree = (*tree)->left;
        return temp;
    } else {
        return del_max_of_tree(&(*tree)->right);
    }
}

void del(bst* *tree, int num)
{
    if (*tree != NULL) {
        if (num < (*tree)->root) {
            del(&(*tree)->left, num);
        } else if (num > (*tree)->root) {
            del(&(*tree)->right, num);
        } else {
            if ((*tree)->left == NULL) {
                *tree = (*tree)->right;
            } else {
                (*tree)->root = del_max_of_tree(&(*tree)->left);
            }
        }
    }
}

_Bool containsp(bst* tree, int num)
{
    if (tree == NULL) {
        return 0;
    } else if (tree->root == num) {
        return 1;
    } else if (num < tree->root) {
        return containsp(tree->left, num);
    } else {
        return containsp(tree->right, num);
    }
}

void printLN(_Bool (*func) (bst*), bst* tree)
{
    func(tree);
    putchar('\n');
}

_Bool _printUp(bst* tree)
{
    if (tree != NULL) {
        if (_printUp(tree->left)) {
            putchar(' ');
        }
        printf("%d", tree->root);
        if (tree->right != NULL) {
            putchar(' ');
        }
        _printUp(tree->right);
        return 1;
    } else {
        return 0;
    }
}

_Bool _printDown(bst* tree)
{
    if (tree != NULL) {
        if (_printDown(tree->right)) {
            putchar(' ');
        }
        printf("%d", tree->root);
        if (tree->left != NULL) {
            putchar(' ');
        }
        _printDown(tree->left);
        return 1;
    } else {
        return 0;
    }
}

_Bool _printLISP(bst* tree)
{
    if (tree == NULL) {
        printf("null");
    } else {
        printf("(%d ", tree->root);
        _printLISP(tree->left);
        putchar(' ');
        _printLISP(tree->right);
        putchar(')');
    }

    return 0;
}

void printUp(bst* tree)
{
    printLN(_printUp, tree);
}

void printDown(bst* tree)
{
    printLN(_printDown, tree);
}

void printLISP(bst* tree)
{
    printLN(_printLISP, tree);
}

int main()
{
    /* Testing
    bst* tree = NULL;

    add(&tree, 3);
    printUp(tree);
    add(&tree, 1);
    printUp(tree);
    add(&tree, 6);
    printUp(tree);
    add(&tree, 0);
    printUp(tree);
    add(&tree, 2);
    printUp(tree);
    printLISP(tree);

    del(&tree, 3);
    printLISP(tree);


    printDown(tree);
    printUp(tree);
    */

    return 0;
}
