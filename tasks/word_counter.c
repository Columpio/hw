#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <locale.h>
#include <time.h>
#define MAX_STRING_LENGTH 30
#define HASH_MAX 30011
#define FILLER "#######################################\n"

/* HASH FUNCTIONS */
unsigned int const_hash(char* key, size_t len)
{
    return 42;
}

unsigned int sum_hash(char* key, size_t len)
{
    int i;
    unsigned int s = 0;

    for (i = 0; i < len; i++) {
        s = (s + key[i]) % UINT_MAX;
    }

    return s;
}

unsigned int hash(char* key, size_t len)
{
    unsigned int hash = 0, i;

    for(i = 0; i < len; i++) {
        hash += key[i];
        hash += hash << 10;
        hash ^= hash >> 6;
    }

    hash += hash << 3;
    hash ^= hash >> 11;
    hash += hash << 15;

    return hash;
}

/* CONS INTERFACE */
struct cons {
    char* str;
    int car;
    struct cons *cdr;
};
typedef struct cons cons;

void cons_add(cons* *list, char* s)
{
    cons *curPtr, *prevPtr, *lastPtr;
    curPtr = *list;

    if (curPtr != NULL) {
        while ((curPtr != NULL) && strcmp(curPtr->str, s)) {
            prevPtr = curPtr;
            curPtr = curPtr->cdr;
        }

        if (curPtr != NULL) {
            ++curPtr->car;
            return;
        }
    }
    lastPtr = malloc(sizeof(cons));
    char* s_cp = malloc(sizeof(char) * (strlen(s) + 1));
    if ((lastPtr == NULL) || (s_cp == NULL)) {
        printf("Not enough memory!\n");
        return;
    }

    strcpy(s_cp, s);
    lastPtr->str = s_cp;
    lastPtr->car = 1;
    lastPtr->cdr = NULL;

    if (*list == NULL) {
        *list = lastPtr;
    } else {
        prevPtr->cdr = lastPtr;
    }
}

void cons_free(cons* *list)
{
    cons *curPtr, *tempPtr;
    curPtr = *list;

    while (curPtr != NULL) {
        tempPtr = curPtr;
        curPtr = curPtr->cdr;
        free(tempPtr->str);
        free(tempPtr);
    }
}

int cons_length(cons* *list)
{
    int length = 0;
    cons *curPtr;
    curPtr = *list;

    while (curPtr != NULL) {
        curPtr = curPtr->cdr;
        ++length;
    }

    return length;
}

/* HASH TABLE INTERFACE */
struct hash_table {

    cons** table;
    int hash_div;
    unsigned int (*hash_func) (char*, size_t);
};
typedef struct hash_table hash_table;

// Tables
void nullfill_hash_table(hash_table* ht)
{
    int i, n = ht->hash_div;

    for (i = 0; i < n; i++) {
        (ht->table)[i] = NULL;
    }
}

hash_table* create_hash_table(unsigned int (*hash_func) (char*, size_t), const int hash_div)
{
    hash_table* ht = malloc(sizeof(hash_table));
    if (ht == NULL) {
        printf("Not enough memory!\n");
        return NULL;
    }

    ht->table = malloc(sizeof(cons) * hash_div);
    if (ht->table == NULL) {
        printf("Not enough memory!\n");
        return NULL;
    }
    ht->hash_div = hash_div;
    ht->hash_func = hash_func;

    nullfill_hash_table(ht);

    return ht;
}

void del_hash_table(hash_table* ht)
{
    int i, n = ht->hash_div;
    cons** table = ht->table;

    for (i = 0; i < n; i++) {
        cons_free(&table[i]);
    }

    free(table);
    free(ht);
}

void pprint_stat(FILE* out, int NnonzeroConses, int SummarConsLength, int min_length,
                 int max_length)
{
    fprintf(out, FILLER);
    fprintf(out, "The number of nonzero conses: %d\n", NnonzeroConses);
    fprintf(out, "The number of elements: %d\n", SummarConsLength);
    fprintf(out, "The average length of conses: %.3f\n",
            NnonzeroConses ? ((double) SummarConsLength) / NnonzeroConses : 0);
    fprintf(out, "The minimum length of the cons: %d\n", min_length);
    fprintf(out, "The maximum length of the cons: %d\n", max_length);
    fprintf(out, "%s\n", FILLER);
}

void print_statistics(hash_table* ht, FILE* out)
{
    int min_length = 0, max_length = 0;
    int curLength, NnonzeroConses = 0, SummarConsLength = 0;
    int i, n = ht->hash_div;
    cons** table = ht->table;

    for (i = 0; i < n; i++) {
        if (table[i] != NULL) {
            ++NnonzeroConses;
            curLength = cons_length(&table[i]);
            SummarConsLength += curLength;
            if (curLength < min_length || !min_length) {
                min_length = curLength;
            }
            if (curLength > max_length) {
                max_length = curLength;
            }
        }
    }

    pprint_stat(out, NnonzeroConses, SummarConsLength, min_length, max_length);
}

int main(void)
{
    setlocale(LC_ALL, "Russian");

    FILE* fl = fopen("Bible2.txt", "r");
    FILE* out = fopen("output.txt", "w");
    if (!fl || !out) {
        printf("File I/O Error\n");
        return -1;
    }

    unsigned int (*func_array[])(char*, size_t) = {const_hash, sum_hash, hash};
    char *func_names[] = {"Constant", "Sum", "Normal Hash"};
    const int number_of_functions = 3;

    char* input = malloc(sizeof(char) * MAX_STRING_LENGTH);
    if (input == NULL) {
        printf("Not enough memory!\n");
        return -1;
    }
    char c, func_num;
    int len = 0;
    int past; // for timing
    input[0] = 0;

    for (func_num = 0; func_num < number_of_functions;  func_num++) {
        hash_table *ht = create_hash_table(func_array[func_num], HASH_MAX);

        past = clock();
        while (!feof(fl)) {
            c = fgetc(fl);
            if ((-64 <= c) && (c <= -1)) {
                if ((-64 <= c) && (c <= -33)) {
                    c += 32;
                }
                input[len++] = c;
                input[len] = 0;
            } else if (input[0]) {
                cons_add(&((ht->table)[func_array[func_num](input, len) % ht->hash_div]), input);
                len = 0;
                input[0] = 0;
            }
        }

        fprintf(out, FILLER);
        fprintf(out, "Time for function %s: %d\n", func_names[func_num], clock() - past);
        print_statistics(ht, out);
        del_hash_table(ht);
        fseek(fl, 0, SEEK_SET);
    }

    fclose(fl);
    fclose(out);

    return 0;
}
