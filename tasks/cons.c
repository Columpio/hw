#include <stdio.h>
#include <stdlib.h>

/* CONS DEFINITION */
struct cons {
    int car;
    struct cons *cdr;
};
typedef struct cons cons;

void cons_add(cons* *list, const int num)
{
    cons *curPtr, *lastPtr;
    lastPtr = malloc(sizeof(cons));
    if (lastPtr == NULL) {
        printf("Not enough memory\n");
        return;
    }

    lastPtr->car = num;
    lastPtr->cdr = *list;

    *list = lastPtr;
}

void cons_del(cons* *list, const int num) // always works, even if num doesn't exist
{
    cons *curPtr, *prevPtr, *tempPtr;

    if (*list != NULL) {
        if ((*list)->car == num) {
            tempPtr = *list;
            *list = (*list)->cdr;
            free(tempPtr);
        } else {
            curPtr = *list;
            do {
                prevPtr = curPtr;
                curPtr = curPtr->cdr;
            } while ((curPtr != NULL) && (curPtr->car != num));

            if (curPtr != NULL) {
                tempPtr = curPtr;
                prevPtr->cdr = curPtr->cdr;
                free(tempPtr);
            }
        }
    }
}

void print_cons(cons* list)
{
    if (list == NULL) {
        printf("NULL\n");
    } else if (list->cdr == NULL) {
        printf("%d\n", list->car);
    } else {
        printf("%d -> ", list->car);
        print_cons(list->cdr);
    }
}

void cycle_cons(cons* *list, int value)
{
    cons *curPtr, *endPtr;
    curPtr = *list;

    if (curPtr != NULL) {
        while ((curPtr->car != value) && (curPtr->cdr != NULL)) {
            curPtr = curPtr->cdr;
        }
        if (curPtr->car == value) {
            endPtr = curPtr;
            while (curPtr->cdr != NULL) {
                curPtr = curPtr->cdr;
            }
            curPtr->cdr = endPtr;
        } else {
            printf("No such key!\n");
        }
    }
}

_Bool print_yes_if_cycle(cons* *list)
{
    cons *turtlePtr, *rabbitPtr;
    turtlePtr = *list;
    rabbitPtr = *list;

    do {
        if ((turtlePtr->cdr == NULL) || (rabbitPtr->cdr == NULL) ||
            ((rabbitPtr->cdr)->cdr == NULL)) {
            printf("No, it's not a cycle!\n");
            return 0;
        }
        turtlePtr = turtlePtr->cdr;
        rabbitPtr = (rabbitPtr->cdr)->cdr;
    } while (turtlePtr != rabbitPtr);

    printf("Yes, it's a cycle!\n");
    return 1;
}

void cons_free(cons* *list)
{
    printf("Trying to quit: ");

    if (print_yes_if_cycle(list)) {
        cons *turtlePtr, *rabbitPtr, *prevPtr;
        turtlePtr = *list;
        rabbitPtr = *list;

        do {
            prevPtr = turtlePtr;
            turtlePtr = turtlePtr->cdr;
            rabbitPtr = (rabbitPtr->cdr)->cdr;
        } while (turtlePtr != rabbitPtr);

        prevPtr->cdr = NULL; // Kills cycle
        cons_free(list);
    } else {
        cons *curPtr, *tempPtr;
        curPtr = *list;

        while (curPtr != NULL) {
            tempPtr = curPtr;
            curPtr = curPtr->cdr;
            free(tempPtr);
        }
    }
}

/* PROGRAM INTERFACE */
void cli(cons* list)
{
    char input;
    int arg;

    while ((input = getchar()) != 'q') {
        switch (input) {
            case 'a': // Adds
                scanf("%d", &arg);
                cons_add(&list, arg);
                break;
            case 'r': // Removes
                scanf("%d", &arg);
                cons_del(&list, arg);
                break;
            case 'p': // Prints
                print_cons(list);
                break;
            case 'c': // makes Cycle
                scanf("%d", &arg);
                cycle_cons(&list, arg);
                break;
            case 't': // Tests, if cycle
                print_yes_if_cycle(&list);
                break;
        }
    }

    cons_free(&list);
}

main()
{
    cons* list = NULL;

    cli(list);

    return 0;
}
