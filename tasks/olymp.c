#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_STRING_LENGTH 100

/* CONS */
struct cons {
    char* str;
    int car;
    struct cons *cdr;
};
typedef struct cons cons;

void cons_add(cons* *list, char* s)
{
    cons *curPtr, *prevPtr, *lastPtr;
    curPtr = *list;

    if (curPtr != NULL) {
        while ((curPtr != NULL) && strcmp(curPtr->str, s)) {
            prevPtr = curPtr;
            curPtr = curPtr->cdr;
        }

        if (curPtr != NULL) {
            ++curPtr->car;
            return;
        }
    }
    lastPtr = malloc(sizeof(cons));
    char* s_cp = malloc(sizeof(char) * (strlen(s) + 1));

    strcpy(s_cp, s);
    lastPtr->str = s_cp;
    lastPtr->car = 1;
    lastPtr->cdr = NULL;

    if (*list == NULL) {
        *list = lastPtr;
    } else {
        prevPtr->cdr = lastPtr;
    }
}

void cons_free(cons* *list)
{
    cons *curPtr, *tempPtr;
    curPtr = *list;

    while (curPtr != NULL) {
        tempPtr = curPtr;
        curPtr = curPtr->cdr;
        free(tempPtr);
    }
}

void print_cons(cons* list)
{
    if (list == NULL) {
        printf("NULL\n");
    } else {
        printf("%s : %d\n", list->str, list->car);
        if (list->cdr != NULL) {
            print_cons(list->cdr);
        }
    }
}

/* HASH */
unsigned int hash(char* key, size_t len)
{
    unsigned int hash = 0, i;

    for(i = 0; i < len; i++) {
        hash += key[i];
        hash += hash << 10;
        hash ^= hash >> 6;
    }

    hash += hash << 3;
    hash ^= hash >> 11;
    hash += hash << 15;

    return hash;
}

void zerofill_hash_table(cons** ht, const int hash_div)
{
    int i;

    for (i = 0; i < hash_div; i++) {
        ht[i] = NULL;
    }
}

void make_hash_table(cons** ht, const int n, const int hash_div)
{
    char* input = malloc(sizeof(char) * MAX_STRING_LENGTH);
    int i;

    zerofill_hash_table(ht, hash_div);

    for (i = 0; i < n; i++) {
        scanf("%s", input);
        cons_add(&ht[hash(input, strlen(input)) % hash_div], input);
    }

    for (i = 0; i < hash_div; i++) {
        if (ht[i] != NULL) {
            print_cons(ht[i]);
        }
        cons_free(&ht[i]);
    }
}

int main()
{
    int n, hash_div;

    scanf("%d", &n);

    hash_div = n < 100000 ? 104729 : n;

    cons** ht = malloc(sizeof(cons) * hash_div);

    make_hash_table(ht, n, hash_div);

    free(ht);

    return 0;
}
