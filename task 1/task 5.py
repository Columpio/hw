def revEQ(a=[]):
    for i in range(len(a) // 2):
        if a[i] != a[-i-1]:
            return False
    return True

if __name__ == '__main__':
    print(revEQ(input().split()))
