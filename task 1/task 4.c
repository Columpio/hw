#include <stdio.h>

int main()
{
   float c = 0;
   _Bool b = 0;
   int n = 0;

   while ((c = getchar()) != '\n') {
      if ((c == '0') && !b) {
         ++n;
         ++b;
      } else {
        b = c != ' ';
      }
   }

   printf("%d\n", n);

   return 0;
}
