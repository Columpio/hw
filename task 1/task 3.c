#include <stdio.h>

double pow(double, int);

double pow(double x, int n)
{
   if (n < 0) {
      if (!x) {
         printf("Zero division\n");
         return -1;
      } else {
         return pow(1 / x, -n);
      }
   } else if (!n) {
      if (!x) {
         printf("NaN\n");
         return -1;
      } else {
         return 1;
      }
   } else if (n % 2) {
      return x * pow(x, n - 1);
   } else {
      return pow(x * x, n / 2);
   }
}

int main()
{
   double x;
   int n;

   scanf("%lf %d", &x, &n);

   printf("%lf\n", pow(x, n));

   return 0;
}
