a = range(2, int(input()) + 1)

for el in a:
    a = [x for x in a if x % el or x is el]

print(' '.join(map(str, a)))
