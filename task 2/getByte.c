int getByte(int x, int n)
{
    return 255 & (x >> (n << 3));
}
