int fitsBits(int x, int n)
{
    int p = x >> (n - 1);

    return !p | !~p;
}
