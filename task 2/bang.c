int bang(int x)
{
    int p = 1 + (x >> 31);

    return (p + (~x + 1 >> 31)) & p;
}
