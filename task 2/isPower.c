int isPower2(int x)
{
    int p = 1 + ~x;

    return !(1 + ~p + (x | p)) & !(1 & (x >> 31)) & !!x;
}
