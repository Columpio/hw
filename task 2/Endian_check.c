_Bool Endian_check(void)
{
    union {
        int x;
        char b[sizeof(int)];
    } c;
    c.x = 1;

    return c.b[0] ? "little" : "big";
}
