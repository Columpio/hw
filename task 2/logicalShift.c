int logicalShift(int x, int n)
{
    int p = !n + ~0;

    return (p & (x >> n) & ((1 << 33 + ~n) + ~0)) + (~p & x);
}
