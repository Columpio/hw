int thirdBits(void)
{
    int x = (36 << 6) | 36; // 12 symbols

    return ((x | (x << 12)) << 6) | 36;
}
