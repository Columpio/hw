int sign(int x)
{
    return (((x >> 31) << 1) + 1) + ~!x + 1;
}
