#include <stdio.h>

void print(int m, int e, int s)
{
    if (e == 255) {
        if (m) {
            printf("NaN");
        } else {
            printf("%cInf", s ? '-' : '+');
        }
    } else {
       printf("(-1) ** %d * 1.%d * 2 ** %d", s, m, e - 127);
    }
    putchar('\n');
}

void preloadAndPrint(int x)
{
    print(x & (1 << 23) - 1,
          x >> 23 & 255,
          x >> 31 & 1);
}

void floatBits1(float y)
{
    union {
       int a;
       float b;
    } c;
    c.b = y;

    preloadAndPrint(c.a);
}

void floatBits2(float y)
{
    union {
        float b;
        struct {
            int m:23;
            int e:8; // -1 == 255
            int s:1; // 0 or -1, needed 0 or 1
        } p;
    } c;

    c.b = y;

    print((1 << 23) - 1 & c.p.m, 255 & c.p.e, -c.p.s); // ^ upper comments
}

void floatBits3(float y)
{
    preloadAndPrint(*((int*) &y));
}

int main()
{
    int a, b;

    scanf("%d %d", &a, &b);

    floatBits1(((float) a) / b);
    floatBits2(((float) a) / b);
    floatBits3(((float) a) / b);

    return 0;
}
