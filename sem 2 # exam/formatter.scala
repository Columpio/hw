/*object Paragraph {
    @annotation.tailrec
    def findMaxWidth(seq: List[String], max: Int = -1): Int =
        if (seq.isEmpty)
            max
        else if (seq.head.length > max)
            findMaxWidth(seq.tail, seq.head.length)
        else findMaxWidth(seq.tail, max)

    @annotation.tailrec
    def adderOfWidth(seq: List[String], maxWidth: Int,
                     out: List[List[Any]] = Nil): List[List[Any]] =
        if (seq.isEmpty)
            out
        else adderOfWidth(seq.tail, maxWidth,
                          out ++ List(List(seq.head, maxWidth - seq.head.length)))

    def addMaxWidth(seq: List[String]): List[List[Any]] = {
        val maxWidth: Int = findMaxWidth(seq)
        adderOfWidth(seq, maxWidth)
    }

    def main(args: Array[String]): Unit = {
        println(addMaxWidth(List("hi", "h", "hello", "")))
    }
}*/

abstract class ParagraphElement {
    override def toString: String
    def length: Int
}

case class Word(val str: String) extends ParagraphElement {
    override def toString: String =
        "W(" + this.str + ")"


    def length: Int = str.length
}

case class Space(val length: Int) extends ParagraphElement {
    override def toString: String =
        "S(" + this.length.toString + ")"
}


abstract class Alignment(var termWidth: Int = 0) {
    def apply(seq: List[ParagraphElement], termWidth: Int):
        List[List[ParagraphElement]]


    @annotation.tailrec
    final def howMuchWordsFits(seq: List[ParagraphElement], leftWidth: Int,
                               count: Int = 0): Int =
        if (seq.isEmpty || seq.head.length > leftWidth)
            count
        else
            howMuchWordsFits(seq.tail,
                             leftWidth - seq.head.length - 1, // 1 counts Spaces
                             count + 1)


    def formatLine(line: List[ParagraphElement]): List[ParagraphElement]


    @annotation.tailrec
    final def seqFormat(seq: List[ParagraphElement],
                  out: List[List[ParagraphElement]] = Nil):
            List[List[ParagraphElement]] =

        if (seq.isEmpty)
            out
        else {
            val wordsForCurLine: Int = howMuchWordsFits(seq, termWidth)

            seqFormat(seq.drop(wordsForCurLine),
                      out ++ List(formatLine(seq.take(wordsForCurLine))))
        }
}

abstract class AlignSide(var leftTab: Boolean = false,
                         var rightTab: Boolean = false) extends Alignment {
    @annotation.tailrec
    final def preFormatLine(line: List[ParagraphElement],
                            out:  List[ParagraphElement] = Nil):
        List[ParagraphElement] =

        if (line.isEmpty)
            if (out.isEmpty) Nil else out.tail // .tail removes extra Space(1)
        else
            preFormatLine(line.tail, out ++ List(new Space(1), line.head))


    final def formatLine(line: List[ParagraphElement]): List[ParagraphElement] = {
        val tempLine: List[ParagraphElement] = preFormatLine(line)
        val leftWidth: Int = termWidth -
            tempLine.foldLeft(0)({(x: Int, y: ParagraphElement) => x + y.length})

        var leftIntend:  Int = 0
        var rightIntend: Int = 0

        if (leftTab)
            if (rightTab) {
                leftIntend = leftWidth / 2
                rightIntend = leftWidth - leftIntend
            } else
                leftIntend = leftWidth
        else {}

        if (leftIntend != 0)
            if (rightIntend != 0)
                new Space(leftIntend) :: tempLine ++ List(new Space(rightIntend))
            else
                new Space(leftIntend) :: tempLine
        else
            tempLine
    }
}

case class AlignLeft() extends AlignSide {
    def apply(seq: List[ParagraphElement], termWidth: Int):
            List[List[ParagraphElement]] = {

        this.termWidth = termWidth
        this.leftTab = false
        this.rightTab = true
        seqFormat(seq)
    }
}

case class AlignRight() extends AlignSide {
    def apply(seq: List[ParagraphElement], termWidth: Int):
            List[List[ParagraphElement]] = {

        this.termWidth = termWidth
        this.leftTab = true
        this.rightTab = false
        seqFormat(seq)
    }
}

case class AlignCenter() extends AlignSide {
    def apply(seq: List[ParagraphElement], termWidth: Int):
            List[List[ParagraphElement]] = {

        this.termWidth = termWidth
        this.leftTab = true
        this.rightTab = true
        seqFormat(seq)
    }
}

case class AlignFill() extends Alignment {
    private def iterFormatLine(line: List[ParagraphElement],
                               spaceLen: Int,
                               finIndex: Int,
                               wordIndex: Int = 0,
                               out: List[ParagraphElement] = Nil):
        List[ParagraphElement] =

        if (line.isEmpty)
            out.tail
        else
            iterFormatLine(line.tail, spaceLen, finIndex, wordIndex + 1,
                           out ++
                           List(new Space(spaceLen + (if (wordIndex <= finIndex)
                                                         1
                                                         else 0)),
                                line.head))


    def formatLine(line: List[ParagraphElement]): List[ParagraphElement] = {
        val leftWidth: Int = termWidth -
            line.foldLeft(0)({(x: Int, y: ParagraphElement) => x + y.length})
        val wordsCount: Int = line.length - 1

        if (wordsCount == 0)
            line
        else
            iterFormatLine(line, leftWidth / wordsCount, leftWidth % wordsCount)
    }


    def apply(seq: List[ParagraphElement], termWidth: Int):
        List[List[ParagraphElement]] = {

        this.termWidth = termWidth
        val rawSeq: List[List[ParagraphElement]] = seqFormat(seq)
        rawSeq.init ++ List(rawSeq.last.map( // last line should be left-orientired
                            {x: ParagraphElement =>
                                x match {
                                    case Space(n) => new Space(1)
                                    case Word(s) => x
                                }
                            }))
    }
}


object Formatter {
    def makeFormated(seq: List[Word], termWidth: Int, howTo: Alignment):
        List[List[ParagraphElement]] =

        howTo match {
            case AlignFill()   => howTo(seq, termWidth)
            case AlignLeft()   => howTo(seq, termWidth)
            case AlignRight()  => howTo(seq, termWidth)
            case AlignCenter() => howTo(seq, termWidth)
    }


    def initFormat(seq: List[String], termWidth: Int,
                   howTo: Alignment): List[List[ParagraphElement]] =
        makeFormated(seq.map({s: String => new Word(s)}), termWidth, howTo)


    def main(args: Array[String]): Unit = {
        println(initFormat(List("he", "me", "ee", "poi", "hi"), 9, AlignFill()))
    }
}
